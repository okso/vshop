/* 商品表添加佣金和红包*/
ALTER TABLE `shopjl_goods`
  ADD COLUMN `brokerage` DECIMAL (10, 2) UNSIGNED DEFAULT 0 NOT NULL COMMENT '分享佣金' AFTER `evaluation_good_star`,
  ADD COLUMN `profit` DECIMAL (10, 2) UNSIGNED DEFAULT 0 NOT NULL COMMENT '红包' AFTER `brokerage` ;
/* 订单增加佣金红包相关字段*/
ALTER TABLE `shopjl_order`
  ADD COLUMN `brokerage` DECIMAL (10, 2) UNSIGNED DEFAULT 0 NOT NULL COMMENT '分享佣金总额' AFTER `shipping_code`,
  ADD COLUMN `profit` DECIMAL (10, 2) UNSIGNED DEFAULT 0 NOT NULL COMMENT '红包总额' AFTER `brokerage`,
  ADD COLUMN `brokerage_account` INT (11) UNSIGNED DEFAULT 0 NOT NULL COMMENT '分享会员id' AFTER `profit`,
  ADD COLUMN `profit_account` INT (11) UNSIGNED NOT NULL COMMENT '红包会员id' AFTER `brokerage_account` ;
ALTER TABLE `shopjl_order_goods`
  ADD COLUMN `brokerage` DECIMAL (10, 2) UNSIGNED DEFAULT 0 NOT NULL COMMENT '分享佣金总额',
  ADD COLUMN `profit` DECIMAL (10, 2) UNSIGNED DEFAULT 0 NOT NULL COMMENT '红包总额' AFTER `brokerage`,
  ADD COLUMN `brokerage_account` INT (11) UNSIGNED DEFAULT 0 NOT NULL COMMENT '分享会员id' AFTER `profit`,
  ADD COLUMN `profit_account` INT (11) UNSIGNED NOT NULL COMMENT '红包会员id' AFTER `brokerage_account` ;

/*会员表增加微信相关信息*/
ALTER TABLE `shopjl_member`
  ADD COLUMN `member_wxopenid` VARCHAR (40) NULL COMMENT '微信openid' AFTER `member_qqinfo`,
  ADD COLUMN `member_wxinfo` TEXT NULL COMMENT '微信信息' AFTER `member_wxopenid`,
  ADD COLUMN `member_wxunionid` VARCHAR (40) NULL COMMENT '微信unionid' AFTER `member_wxinfo`,
  ADD COLUMN `recommend_id` INT(11) UNSIGNED DEFAULT 0 NOT NULL COMMENT '推荐人会员编号' AFTER `member_exppoints`, ADD COLUMN `share_id` CHAR(32) DEFAULT '0' NOT NULL COMMENT '分享码' AFTER `recommend_id`,
  ADD INDEX `wxopenid` (`member_wxopenid`);

/*分享佣金账户表*/
CREATE TABLE `shopjl_bs_brokerage` (
  `bk_id` INT(11) NOT NULL AUTO_INCREMENT COMMENT '自增编号',
  `bk_member_id` INT(11) NOT NULL COMMENT '会员编号',
  `bk_member_name` VARCHAR(50) NOT NULL COMMENT '会员名称',
  `bk_amount` DECIMAL(10,2) NOT NULL DEFAULT '0.00' COMMENT '金额',
  `bk_bank_name` VARCHAR(40) NOT NULL COMMENT '收款银行',
  `bk_bank_no` VARCHAR(30) DEFAULT NULL COMMENT '收款账号',
  `bk_bank_user` VARCHAR(10) DEFAULT NULL COMMENT '开户人姓名',
  `bk_id_number` CHAR(18) DEFAULT NULL COMMENT '身份证号',
  PRIMARY KEY (`bk_id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8 COMMENT='分享佣金表';

/*佣金/红包记录*/
CREATE TABLE `shopjl_bs_profit_log` (
  `profit_log_id` BIGINT NOT NULL AUTO_INCREMENT,
  `order_id` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '订单',
  `goods_id` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '商品',
  `store_id` INT (11) UNSIGNED NOT NULL COMMENT '店铺',
  `member_id` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '用户id(受益人)',
  `type` TINYINT (1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '1佣金,2红包',
  `amount` DECIMAL (10, 2) NOT NULL DEFAULT 0 COMMENT '总金额',
  `note` VARCHAR (200) NOT NULL COMMENT '备注',
  `modified` INT (10) UNSIGNED NOT NULL COMMENT '修改时间',
  `dateline` INT (10) UNSIGNED NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`profit_log_id`)
) ENGINE = INNODB CHARSET = utf8 COLLATE = utf8_general_ci COMMENT = '佣金/红包记录' ;

/* 佣金提现申请*/
CREATE TABLE `shopjl_bs_brokerage_apply` (
  `apply_id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `member_id` INT (11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '申请人id',
  `amount` DECIMAL (10, 2) NOT NULL DEFAULT 0 COMMENT '申请金额',
  `apply_time` INT (10) UNSIGNED NOT NULL COMMENT '申请时间',
  `bk_bank_name` VARCHAR (40) NOT NULL DEFAULT '' COMMENT '银行名称',
  `bk_bank_no` VARCHAR (30) NOT NULL DEFAULT '' COMMENT '账号',
  `bk_bank_user` VARCHAR (10) NOT NULL DEFAULT '' COMMENT '开户姓名',
  `verify_id` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '审核人编号',
  `verify_time` INT (10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '审核时间',
  `transfer_id` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '转账人编号',
  `transfer_time` INT (10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '转账时间',
  `transfer_no` VARCHAR (40) NOT NULL DEFAULT '' COMMENT '转账流水号',
  `status` TINYINT NOT NULL DEFAULT 0 COMMENT '状态0申请中,1已审核,-1已驳回',
  `modified` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '修改时间',
  `dateline` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  PRIMARY KEY (`apply_id`)
) ENGINE = INNODB CHARSET = utf8 COLLATE = utf8_general_ci COMMENT = '佣金提现申请';

CREATE TABLE `shopjl_bs_brokerage_apply_log` (
  `log_id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `apply_id` BIGINT UNSIGNED NOT NULL DEFAULT 0 COMMENT '申请id',
  `status` TINYINT NOT NULL DEFAULT 0 COMMENT '当前申请状态',
  `op_type` TINYINT UNSIGNED NOT NULL DEFAULT 0 COMMENT '操作人类型0普通用户,1管理员,',
  `op_id` INT NOT NULL DEFAULT 0 COMMENT '操作人id',
  `amount` DECIMAL (10, 2) NOT NULL DEFAULT 0 COMMENT '影响金额',
  `note` VARCHAR (200) NOT NULL DEFAULT '' COMMENT '备注',
  `dateline` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  PRIMARY KEY (`log_id`)
) ENGINE = INNODB CHARSET = utf8 COLLATE = utf8_general_ci ;

/*商品浏览量*/
CREATE TABLE `shopjl_bs_goods_view` (
  `goods_id` INT (10) NOT NULL COMMENT '商品编号',
  `year_month` INT (10) NOT NULL COMMENT '年月',
  `month_view` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '月浏览量',
  `d01` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d02` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d03` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d04` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d05` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d06` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d07` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d08` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d09` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d10` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d11` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d12` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d13` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d14` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d15` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d16` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d17` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d18` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d19` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d20` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d21` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d22` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d23` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d24` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d25` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d26` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d27` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d28` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d29` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d30` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d31` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  PRIMARY KEY (`goods_id`, `year_month`)
) ENGINE = MYISAM CHARSET = utf8 COLLATE = utf8_general_ci COMMENT = '商品浏览量' ;

/*分享浏览量*/
CREATE TABLE `shopjl_bs_share_view` (
  `member_id` INT (11) NOT NULL DEFAULT 0 COMMENT '商品编号',
  `goods_id` INT (11) NOT NULL DEFAULT 0 COMMENT '商品编号',
  `year_month` INT (10) NOT NULL COMMENT '年月',
  `url_md5` CHAR (32) NOT NULL DEFAULT '' COMMENT '分享链接md5值',
  `url` VARCHAR (100) NOT NULL DEFAULT '' COMMENT '分享链接',
  `month_view` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '月浏览量',
  `d01` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d02` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d03` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d04` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d05` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d06` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d07` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d08` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d09` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d10` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d11` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d12` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d13` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d14` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d15` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d16` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d17` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d18` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d19` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d20` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d21` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d22` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d23` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d24` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d25` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d26` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d27` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d28` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d29` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d30` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  `d31` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '日浏览量(数字是日期)',
  PRIMARY KEY (`member_id`, `year_month`,`url_md5`,`goods_id`)
) ENGINE = MYISAM CHARSET = utf8 COLLATE = utf8_general_ci COMMENT = '分享浏览量' ;