<?php
/**
 * mobile父类
 *
 *
 * @copyright  Copyright (c) 2007-2013 BesonIT Inc. (http://www.BesonIT.net)
 * @license    http://www.BesonIT.net
 * @link       http://www.BesonIT.net
 * @since      File available since Release v1.1
 */

use Shopnc\Tpl;

defined('BYshopJL') or exit('Access Invalid!');

/********************************** 前台control父类 **********************************************/

class mobileControl{

    //客户端类型
    protected $client_type_array = array('android', 'wap', 'wechat', 'ios');
    //列表默认分页数
    protected $page = 5;


	public function __construct() {
        Language::read('mobile');

        //分页数处理
        $page = intval($_GET['page']);
        if($page > 0) {
            $this->page = $page;
        }
        echo '0<br>';
        $wx= new weixin(2);
        echo '1<br>';
        if(empty($wx->code)){
            echo '2<br>';
            $wx->webAuthGetCode();
        }
        echo '3<br>';
        $result=$wx->webAuthGetAccessToken();
        echo '4<br>';
        var_dump($result);
        exit;
    }
}

class mobileHomeControl extends mobileControl{
	public function __construct() {
        parent::__construct();
    }
}

class mobileMemberControl extends mobileControl{

    protected $member_info = array();

	public function __construct() {
        parent::__construct();

        $model_mb_user_token = Model('mb_user_token');
        $key = $_POST['key'];
        if(empty($key)) {
            $key = $_GET['key'];
        }
        $mb_user_token_info = $model_mb_user_token->getMbUserTokenInfoByToken($key);
        if(empty($mb_user_token_info)) {
            output_error('请登录', array('login' => '0'));
        }

        $model_member = Model('member');
        $this->member_info = $model_member->getMemberInfoByID($mb_user_token_info['member_id']);
        $this->member_info['client_type'] = $mb_user_token_info['client_type'];
        if(empty($this->member_info)) {
            output_error('请登录', array('login' => '0'));
        } else {
            //读取卖家信息
            $seller_info = Model('seller')->getSellerInfo(array('member_id'=>$this->member_info['member_id']));
            $this->member_info['store_id'] = $seller_info['store_id'];
        }
    }
}
